const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const defaultConfig = {
  devtool: 'eval',
  entry: './src/js/index.js',
  output: {
    path: path.join(__dirname, '../../dist'),
    publicPath: '/',
    filename: '[name].js',
  },
  module: {
    loaders: [
      { test: /\.jsx?$/, exclude: /node_modules\/(?!item-builder)/, loader: 'babel-loader' },
      { test: /\.json$/, loader: 'json-loader' },
      { test: /\.css$/, loader: ExtractTextPlugin.extract('style-loader', 'css-loader') },
      {
        test: /\.woff(\?-5joh1s)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff&name=[path][name].[ext]',
      }, {
        test: /\.woff2(\?-5joh1s)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff2&name=[path][name].[ext]',
      }, {
        test: /\.(eot|ttf|svg|gif|png|jpg)(\?-5joh1s)?$/,
        loader: 'file-loader',
      },
    ],
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.json'],
  },
  plugins: [
    new ExtractTextPlugin('[contenthash].css'),
  ],
};

module.exports = defaultConfig;
